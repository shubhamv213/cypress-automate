/// <reference types="Cypress" />


const checkAllSteps = () => {

    return cy.get('input[type="hidden"]').invoke('val')
        .then(text => {
            cy.log(text);
            if (text === "no-default-selected") {
                noDefaultSelected();
                checkAllSteps();
            }
            if (text === "mediaUpload") {
                fileUpload();
                checkAllSteps();
            }
            if (text === "subReason") {
                subReason();
                checkAllSteps();
            }
            if (text === "same_variant") {
                sameVariant();
            }
            if (text === "all_variant") {
                allVariant();
            }
            if (text === "across_dimension-show_info") {
                acrossDimensionShowInfo();
            }
            if (text === "across_dimension-list") {
                acrossDimensionList();
            }
            if (text === "return") {
                returnSelect();
            }
            if (text === "product_catalog") {
                productCatalog();
            }

        });
}
const noDefaultSelected = () => {
    cy.get("#order-service-modal > div > div > div.order-service__content > div.service-mode > ul > li:nth-child(1)").click();
}
const allVariant = () => {
    cy.get('.service-list > [data-cy-servicelist="all_variant"] > .service-list-item-type')
        .click();
    cy.get("#available-Size-variant").then($body => {
        if ($body.find('div > select')) {
            cy.get("#available-Size-variant > div > select").select('M')
        }
        else {
            cy.get("#available-Size-variant>button").eq(1).click()
        }
    })
    cy.get('[data-cy-button="all_variant"]').click();
}
const sameVariant = () => {
    cy.get('.service-list > [data-cy-servicelist="same_variant"] > .service-list-item-type').click()
    cy.get('[data-cy-button="same_variant"]').click();
}
const fileUpload = () => {
    //  Upload File
    return cy.fixture('logo.png', 'base64').then(fileContent => {
        cy.get('#fileElemdddd', { force: true }).upload(
            { fileContent, fileName: 'logo.png', mimeType: 'image/png' }
        ).then(el => {
            cy.wait(4000);
            cy.get('div.order-service__content > div.upload-photos > div.actions.no-gutters.row > div > button').click();

        })

    });
}
const acrossDimensionShowInfo = () => {
    cy.get('div.order-service__content > div.service-mode > ul > li:nth-child(1) > div.select-variant.collapse.show > div > div > div > div > div.variantTile__img').click();
    cy.get('[data-cy-button="across_dimension-show_info"]').click();
}
const acrossDimensionList = () => {
    //list
    cy.get('.service-list > [data-cy-servicelist="across_dimension-list"] > .service-list-item-type')
        .click()
    cy.get("div.select-variant.collapse.show > div > div > button").eq(1).click();
    cy.get('[data-cy-button="across_dimension-list"]').click();
}
const subReason = () => {
    return cy
        .get('.sub-reason > .sub-reason-list >li').eq(0).click();

}
const returnSelect = () => {
    cy.get('[data-cy-button="return"]').click();
    cy.get('[data-cy-button="action-button"] > span').click();
}
const productCatalog = () => {
    cy.get('.service-list > [data-cy-servicelist="product_catalog"] > .service-list-item-type')
        .click()
    cy.get('[data-cy-button="product_catalog"]').click();
    cy.get('.catalog-list > .catalog-item', { force: true }).eq(6).click();
    cy.get("#available-Size-variant").then($body => {
        if ($body.find('div > select')) {
            cy.get("#available-Size-variant > div > select").select('M')
        }
        else {
            cy.get("#available-Size-variant>button").eq(1).click()
        }
    })
    cy.get('div.order-service__content > div.products-catalogue > div.actions.no-gutters.row > div > button', { multiple: true }).click()
}
const finalSteps = () => {


    cy.get('[data-cy-button="confirm-button"] > span', { multiple: true }).click();
    cy.wait('@dataGetFirst').its('status').should('be', 200);

    cy.contains('Continue').should('not.be.disabled');

    cy.get('#order-service > div.refund-shipping > div.row.justify-content-center.bottom-padding.no-gutters > div > div > div.button-style > button').click();

    cy.wait('@dataGetFirst').its('status').should('be', 200);

    cy.get('[data-cy-button="action-button"] > span').click().end();
}
//Across dimension
context('Basic Test', () => {
    beforeEach(function () {

        cy.visit(Cypress.env("baseUrl"));

        cy.server().route("GET", Cypress.env("orderServiceInit")).as('orderServiceInit');
        cy.server().route("GET", Cypress.env("orderService")).as('dataGetFirst');
    });

    it("Select not required any more as return", () => {
        cy.get('#email')
            .type("james.saxon10@gmail.com")
            .should('have.value', "james.saxon10@gmail.com")
        cy.get('#retailerOrderId')
            .type("AR-29595")
            .should('have.value', "AR-29595")
        cy.get(".LoginForm__button").click();
        cy.wait('@orderServiceInit').its('status').should('be', 200);
        cy.url()
            .should('include', '/orderService/AR-29595');
        cy.get(':nth-child(1) > .no-gutters > .action-col > .button-div > [data-cy=select-button] > span').click();
        cy.get('.reason').eq(0)
            .then(element => element.click());

        // Find InWhich Screen
        checkAllSteps();
        finalSteps();
    })
})


//Find Length
        // cy.get('.reason')
        //     .then(listing => {
        //         const listingCount = Cypress.$(listing).length;
        //         console.log(listingCount)
        //     });
//
        // cy.get('[data-cy-name="Changed my mind"] > .reason-head > .reason-type', { multiple: true }).click();
        // cy.contains('Exchange with another product from the store', { multiple: true }).click();
